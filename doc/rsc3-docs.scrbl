#lang scribble/manual

@title{rsc3: racket/scheme/supercollider}

@table-of-contents[]

Partially regenerated documentation (rsc.help.scm -> rsc.scrbl)

@; ------------------------------------------------------------------------
@include-section["help/server-command/index.scrbl"]
@include-section["help/ugen/index.scrbl"]
@include-section["tutorials/index.scrbl"]

@index-section[]
