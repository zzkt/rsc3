# -*- mode: org;  coding: utf-8; -*-
#+title: UGEN names

These are the ugens (etc) mapped from SUperCOllider to scheme names. As found in =rsc3/main.rkt=
* filters

| *scheme*               | *SuperCollider*     | *scheme args*                            |
| allpass-c            | AllpassC          | in max-dt dt decay                     |
| allpass-l            | AllpassL          | in max-dt dt decay                     |
| allpass-n            | AllpassN          | in max-dt dt decay                     |
| amp-comp             | AmpComp           | freq root exp                          |
| amp-comp-a           | AmpCompA          | freq root min-amp root-amp             |
| apf                  | APF               | in freq radius                         |
| balance2             | Balance2          | left right pos level                   |
| ball                 | Ball              | in g damp friction                     |
| bi-pan-b2            | BiPanB2           | in-a in-b azimuth gain                 |
| bpf                  | BPF               | in freq rq                             |
| bpz2                 | BPZ2              | in                                     |
| brf                  | BRF               | in freq rq                             |
| brz2                 | BRZ2              | in                                     |
| buf-allpass-c        | BufAllpassC       | buf in dt decay                        |
| buf-allpass-l        | BufAllpassL       | buf in dt decay                        |
| buf-allpass-n        | BufAllpassN       | buf in dt decay                        |
| buf-comb-c           | BufCombC          | buf in dt decay                        |
| buf-comb-l           | BufCombL          | buf in dt decay                        |
| buf-comb-n           | BufCombN          | buf in dt decay                        |
| buf-delay-c          | BufDelayC         | buf in dt                              |
| buf-delay-l          | BufDelayL         | buf in dt                              |
| buf-delay-n          | BufDelayN         | buf in dt                              |
| buf-wr               | BufWr             | bufnum phase loop input-array          |
| clip                 | Clip              | in lo hi                               |
| coin-gate            | CoinGate          | prob in                                |
| comb-c               | CombC             | in max-dt dt decay                     |
| comb-l               | CombL             | in max-dt dt decay                     |
| comb-n               | CombN             | in max-dt dt decay                     |
| compander            | Compander         | in ctl thr sl-b sl-a cl-tm rx-tm       |
| compander-d          | CompanderD        | in thr sl-b sl-a cl-tm rx-tm           |
| decay                | Decay             | in decay-time                          |
| decay2               | Decay2            | in attack-time decay-time              |
| decode-b2            | DecodeB2          | w x y orientation                      |
| degree-to-key        | DegreeToKey       | bufnum in octave                       |
| delay-c              | DelayC            | in max-dt dt                           |
| delay-l              | DelayL            | in max-dt dt                           |
| delay-n              | DelayN            | in max-dt dt                           |
| delay1               | Delay1            | in                                     |
| delay2               | Delay2            | in                                     |
| demand               | Demand            | trig reset demand-ugens                |
| detect-silence       | DetectSilence     | in amp time done-action                |
| disk-out             | DiskOut           | bufnum array                           |
| done                 | Done              | src                                    |
| fold                 | Fold              | in lo hi                               |
| formlet              | Formlet           | in freq attacktime decay               |
| fos                  | FOS               | in a0 a1 b1                            |
| free                 | Free              | in node-id                             |
| free-self            | FreeSelf          | in                                     |
| free-self-when-done  | FreeSelfWhenDone  | in                                     |
| free-verb            | FreeVerb          | in mix room damp                       |
| free-verb2           | FreeVerb2         | in1 in2 mix room damp                  |
| gate                 | Gate              | in trig                                |
| hasher               | Hasher            | in                                     |
| hilbert              | Hilbert           | in) 2                                  |
| hpf                  | HPF               | in freq                                |
| hpz1                 | HPZ1              | in                                     |
| hpz2                 | HPZ2              | in                                     |
| image-warp           | ImageWarp         | pic x y interpolation-type             |
| in-range             | InRange           | in lo hi                               |
| in-rect              | InRect            | x y rect                               |
| index                | Index             | bufnum in                              |
| integrator           | Integrator        | in coef                                |
| klank                | Klank             | i f-scale f-offset dscale spec         |
| lag                  | Lag               | in lag-time                            |
| lag-control          | LagControl        |                                        |
| lag2                 | Lag2              | in lag-time                            |
| lag3                 | Lag3              | in lag-time                            |
| last-value           | LastValue         | in diff                                |
| latch                | Latch             | in trig                                |
| leak-dc              | LeakDC            | in coef                                |
| least-change         | LeastChange       | a b                                    |
| limiter              | Limiter           | in level dur                           |
| lin-exp              | LinExp            | in srclo srchi dstlo dsthi             |
| lin-pan2             | LinPan2           | in pos level                           |
| lin-x-fade2          | LinXFade2         | in-a in-b pan level                    |
| linen                | Linen             | gate atk-tm sus-lvl rel-tm done-action |
| local-out            | LocalOut          | array                                  |
| lpf                  | LPF               | in freq                                |
| lpz1                 | LPZ1              | in                                     |
| lpz2                 | LPZ2              | in                                     |
| mantissa-mask        | MantissaMask      | in bits                                |
| median               | Median            | length in                              |
| mid-eq               | MidEq             | in freq rq db                          |
| moog-ff              | MoogFF            | in freq gain reset                     |
| most-change          | MostChange        | a b                                    |
| mul-add              | MulAdd            | a b c                                  |
| normalizer           | Normalizer        | in level dur                           |
| offset-out           | OffsetOut         | bus inputs                             |
| one-pole             | OnePole           | in coef                                |
| one-zero             | OneZero           | in coef                                |
| out                  | Out               | bus inputs                             |
| pan-az               | PanAz             | nc in pos lvl wdth orientation         |
| pan-b                | PanB              | in azimuth elevation gain              |
| pan-b2               | PanB2             | in azimuth gain                        |
| pan2                 | Pan2              | in pos level                           |
| pan4                 | Pan4              | in xpos ypos level                     |
| pause                | Pause             | in node-id                             |
| pause-self           | PauseSelf         | in                                     |
| pause-self-when-done | PauseSelfWhenDone | in                                     |
| peak                 | Peak              | trig reset                             |
| peak-follower        | PeakFollower      | in decay                               |
| pitch-shift          | PitchShift        | in win-sz p-rt p-dp t-dp               |
| pluck                | Pluck             | in trig max-dt dt decay coef           |
| poll                 | Poll              | trig in trig-id label                  |
| pulse-count          | PulseCount        | trig reset                             |
| pulse-divider        | PulseDivider      | trig div start                         |
| ramp                 | Ramp              | in lag-time                            |
| record-buf           | RecordBuf         | b off rl pl r lp tr i                  |
| replace-out          | ReplaceOut        | bus inputs                             |
| resonz               | Resonz            | in freq bwr                            |
| rhpf                 | RHPF              | in freq rq                             |
| ringz                | Ringz             | in freq decay                          |
| rlpf                 | RLPF              | in freq rq                             |
| rotate2              | Rotate2           | x y pos                                |
| running-max          | RunningMax        | in trig                                |
| running-min          | RunningMin        | in trig                                |
| running-sum          | RunningSum        | in numsamp                             |
| schmidt              | Schmidt           | in lo hi                               |
| scope-out            | ScopeOut          | input-array bufnum                     |
| select               | Select            | which array                            |
| send-trig            | SendTrig          | in id value                            |
| set-reset-ff         | SetResetFF        | trig reset                             |
| shaper               | Shaper            | bufnum in                              |
| silent               | Silent            |                                        |
| slew                 | Slew              | in up dn                               |
| slope                | Slope             | in                                     |
| sos                  | SOS               | in a0 a1 a2 b1 b2                      |
| spring               | Spring            | in spring damp                         |
| stepper              | Stepper           | trig reset min max step resetval       |
| sweep                | Sweep             | trig rate                              |
| t-ball               | TBall             | in g damp friction                     |
| t-delay              | TDelay            | in dur                                 |
| t-exp-rand           | TExpRand          | lo hi trig                             |
| t-grains             | TGrains           | tr b rt c-pos dur pan amp interp       |
| t-pulse              | TPulse            | trig freq width                        |
| t-rand               | TRand             | lo hi trig                             |
| ti-rand              | TIRand            | lo hi trig                             |
| timer                | Timer             | trig                                   |
| toggle-ff            | ToggleFF          | trig                                   |
| trapezoid            | Trapezoid         | in a b c d                             |
| trig                 | Trig              | in dur                                 |
| trig1                | Trig1             | in dur                                 |
| tw-index             | TWindex           | in normalize array                     |
| two-pole             | TwoPole           | in freq radius                         |
| two-zero             | TwoZero           | in freq radius                         |
| vibrato              | Vibrato           | f rt dpth dly onset rvar dvar iphase   |
| wrap                 | Wrap              | in lo hi                               |
| wrap-index           | WrapIndex         | bufnum in                              |
| x-fade2              | XFade2            | in-a in-b pan level                    |
| x-out                | XOut              | bus xfade inputs                       |
| xy                   | XY                | xscale yscale xoff yoff rot rate       |
| zero-crossing        | ZeroCrossing      | in                                     |

* oscillators

| *scheme*          | *SuperCollider* | *scheme args*                           |
| amplitude       | Amplitude     | in atk-tm rel-tm                      |
| blip            | Blip          | freq numharm                          |
| brown-noise     | BrownNoise    |                                       |
| buf-channels    | BufChannels   | buf                                   |
| buf-dur         | BufDur        | buf                                   |
| buf-frames      | BufFrames     | buf                                   |
| buf-rate-scale  | BufRateScale  | buf                                   |
| buf-rd          | BufRd         | bufnum phase loop interp              |
| buf-sample-rate | BufSampleRate | buf                                   |
| buf-samples     | BufSamples    | buf                                   |
| c-osc           | COsc          | bufnum freq beats                     |
| clip-noise      | ClipNoise     |                                       |
| crackle         | Crackle       | chaos-param                           |
| cusp-l          | CuspL         | freq a b xi                           |
| cusp-n          | CuspN         | freq a b xi                           |
| demand-env-gen  | DemandEnvGen  | l d s c g r ls lb ts da               |
| disk-in         | DiskIn        | bufnum                                |
| dust            | Dust          | density                               |
| dust2           | Dust2         | density                               |
| duty            | Duty          | dur reset da lvl                      |
| env-gen         | EnvGen        | g ls lb ts da spec                    |
| f-sin-osc       | FSinOsc       | freq iphase                           |
| fb-sine-c       | FBSineC       | freq im fb a c xi yi                  |
| fb-sine-l       | FBSineL       | freq im fb a c xi yi                  |
| fb-sine-n       | FBSineN       | freq im fb a c xi yi                  |
| formant         | Formant       | fundfreq formfreq bwfreq              |
| gbman-c         | GbmanC        | freq xi yi                            |
| gbman-l         | GbmanL        | freq xi yi                            |
| gbman-n         | GbmanN        | freq xi yi                            |
| gendy1          | Gendy1        | ad dd adp ddp mnf mxf as ds ic kn     |
| gendy2          | Gendy2        | ad dd adp ddp mnf mxf as ds ic kn a c |
| gendy3          | Gendy3        | ad dd adp ddp f as ds ic kn           |
| gray-noise      | GrayNoise     |                                       |
| henon-c         | HenonC        | freq a b x0 x1                        |
| henon-l         | HenonL        | freq a b x0 x1                        |
| henon-n         | HenonN        | freq a b x0 x1                        |
| impulse         | Impulse       | freq phase                            |
| in              | In            | bus                                   |
| key-state       | KeyState      | key min max lag                       |
| klang           | Klang         | freqscale freqoffset spec-array       |
| latoocarfian-c  | LatoocarfianC | freq a b c d xi yi                    |
| latoocarfian-l  | LatoocarfianL | freq a b c d xi yi                    |
| latoocarfian-n  | LatoocarfianN | freq a b c d xi yi                    |
| lf-clip-noise   | LFClipNoise   | freq                                  |
| lf-cub          | LFCub         | freq iphase                           |
| lf-noise0       | LFNoise0      | freq                                  |
| lf-noise1       | LFNoise1      | freq                                  |
| lf-noise2       | LFNoise2      | freq                                  |
| lf-par          | LFPar         | freq iphase                           |
| lf-pulse        | LFPulse       | freq iphase width                     |
| lf-saw          | LFSaw         | freq iphase                           |
| lf-tri          | LFTri         | freq iphase                           |
| lfd-clip-noise  | LFDClipNoise  | freq                                  |
| lfd-noise0      | LFDNoise0     | freq                                  |
| lfd-noise1      | LFDNoise1     | freq                                  |
| lfd-noise3      | LFDNoise3     | freq                                  |
| lin-cong-c      | LinCongC      | freq a c m xi                         |
| lin-cong-l      | LinCongL      | freq a c m xi                         |
| lin-cong-n      | LinCongN      | freq a c m xi                         |
| line            | Line          | start end dur done-action             |
| local-in        | LocalIn       |                                       |
| logistic        | Logistic      | chaos-param freq                      |
| lorenz-l        | LorenzL       | freq s r b h xi yi zi                 |
| mouse-button    | MouseButton   | minval maxval lag                     |
| mouse-x         | MouseX        | min max warp lag                      |
| mouse-y         | MouseY        | min max warp lag                      |
| noah-noise      | NoahNoise     |                                       |
| osc             | Osc           | bufnum freq phase                     |
| osc-n           | OscN          | bufnum freq phase                     |
| p-sin-grain     | PSinGrain     | freq dur amp                          |
| phasor          | Phasor        | trig rate start end reset-pos         |
| pink-noise      | PinkNoise     |                                       |
| pulse           | Pulse         | freq width                            |
| quad-c          | QuadC         | freq a b c xi                         |
| quad-l          | QuadL         | freq a b c xi                         |
| quad-n          | QuadN         | freq a b c xi                         |
| rand-id         | RandID        | id                                    |
| rand-seed       | RandSeed      | trig seed                             |
| saw             | Saw           | freq                                  |
| shared-in       | SharedIn      |                                       |
| sin-osc         | SinOsc        | freq phase                            |
| sin-osc-fb      | SinOscFB      | freq feedback                         |
| standard-l      | StandardL     | freq k xi yi                          |
| standard-n      | StandardN     | freq k xi yi                          |
| sync-saw        | SyncSaw       | sync-freq saw-freq                    |
| t-duty          | TDuty         | dur reset done-action level gap       |
| trig-control    | TrigControl   |                                       |
| v-osc           | VOsc          | bufpos freq phase                     |
| v-osc3          | VOsc3         | bufpos freq1 freq2 freq3              |
| var-saw         | VarSaw        | freq iphase width                     |
| white-noise     | WhiteNoise    |                                       |
| x-line          | XLine         | start end dur done-action             |


* specialized

| *scheme*              | *SuperCollider*      | *scheme args*                       | *rate* |
| control-rate        | ControlRate        |                                   | ir   |
| convolution         | Convolution        | in kernel frame-size              | ar   |
| convolution2        | Convolution2       | in b tr frame-size                | ar   |
| dbrown              | Dbrown             | length lo hi step                 | dr   |
| dbufrd              | Dbufrd             | bufnum phase loop                 | dr   |
| dgeom               | Dgeom              | length start grow                 | dr   |
| dibrown             | Dibrown            | length lo hi step                 | dr   |
| diwhite             | Diwhite            | length lo hi                      | dr   |
| drand               | Drand              | length array                      | dr   |
| dseq                | Dseq               | length array                      | dr   |
| dser                | Dser               | length array                      | dr   |
| dseries             | Dseries            | length start step                 | dr   |
| dswitch             | Dswitch            | length array                      | dr   |
| dswitch1            | Dswitch1           | length array                      | dr   |
| dwhite              | Dwhite             | length lo hi                      | dr   |
| dxrand              | Dxrand             | length array                      | dr   |
| exp-rand            | ExpRand            | lo hi                             | ir   |
| fft                 | FFT                | buf in hop wintype active winsize | kr   |
| grain-buf           | GrainBuf           | tr dur sndb rt ps i pan envb      | ar   |
| grain-fm            | GrainFM            | tr dur cf mf indx pan envb        | ar   |
| grain-in            | GrainIn            | tr dur in pan envbuf              | ar   |
| grain-sin           | GrainSin           | tr dur freq pan envbuf            | ar   |
| i-rand              | IRand              | lo hi                             | ir   |
| ifft                | IFFT               | buf wintype winsize               | ar   |
| in-feedback         | InFeedback         | bus                               | ar   |
| in-trig             | InTrig             | bus                               | kr   |
| k2a                 | K2A                | in                                | ar   |
| lag-in              | LagIn              | bus lag                           | kr   |
| lin-rand            | LinRand            | lo hi minmax                      | ir   |
| n-rand              | NRand              | lo hi n                           | ir   |
| num-audio-buses     | NumAudioBuses      |                                   | ir   |
| num-buffers         | NumBuffers         |                                   | ir   |
| num-control-buses   | NumControlBuses    |                                   | ir   |
| num-input-buses     | NumInputBuses      |                                   | ir   |
| num-output-buses    | NumOutputBuses     |                                   | ir   |
| num-running-synths  | NumRunningSynths   |                                   | ir   |
| pack-fft            | PackFFT            | b sz fr to z mp                   | kr   |
| pitch               | Pitch              | in if mnf mxf ef mxb m at pt ds   | kr   |
| play-buf            | PlayBuf            | b rt tr start loop                | ar   |
| pv-add              | PV_Add             | buf-a buf-b                       | kr   |
| pv-bin-scramble     | PV_BinScramble     | b wipe width trig                 | kr   |
| pv-bin-shift        | PV_BinShift        | b stretch shift                   | kr   |
| pv-bin-wipe         | PV_BinWipe         | b-a b-b wipe                      | kr   |
| pv-brick-wall       | PV_BrickWall       | b wipe                            | kr   |
| pv-conformal-map    | PV_ConformalMap    | b real imag                       | kr   |
| pv-copy             | PV_Copy            | b-a b-b                           | kr   |
| pv-copy-phase       | PV_CopyPhase       | b-a b-b                           | kr   |
| pv-diffuser         | PV_Diffuser        | b trig                            | kr   |
| pv-hainsworth-foote | PV_HainsworthFoote | b h f t w                         | ar   |
| pv-jensen-andersen  | PV_JensenAndersen  | b c e f s t w                     | ar   |
| pv-local-max        | PV_LocalMax        | b threshold                       | kr   |
| pv-mag-above        | PV_MagAbove        | b threshold                       | kr   |
| pv-mag-below        | PV_MagBelow        | b threshold                       | kr   |
| pv-mag-clip         | PV_MagClip         | b threshold                       | kr   |
| pv-mag-freeze       | PV_MagFreeze       | b freeze                          | kr   |
| pv-mag-mul          | PV_MagMul          |                                   | kr   |
| pv-mag-noise        | PV_MagNoise        | b                                 | kr   |
| pv-mag-shift        | PV_MagShift        |                                   | kr   |
| pv-mag-smear        | PV_MagSmear        | b bins                            | kr   |
| pv-mag-squared      | PV_MagSquared      |                                   | kr   |
| pv-max              | PV_Max             |                                   | kr   |
| pv-min              | PV_Min             |                                   | kr   |
| pv-mul              | PV_Mul             |                                   | kr   |
| pv-phase-shift      | PV_PhaseShift      | b shift                           | kr   |
| pv-phase-shift270   | PV_PhaseShift270   | b                                 | kr   |
| pv-phase-shift90    | PV_PhaseShift90    | b                                 | kr   |
| pv-rand-comb        | PV_RandComb        | b wipe trig                       | kr   |
| pv-rand-wipe        | PV_RandWipe        | b-a b-b wipe trig                 | kr   |
| pv-rect-comb        | PV_RectComb        | b nt phase width                  | kr   |
| pv-rect-comb2       | PV_RectComb2       |                                   | kr   |
| radians-per-sample  | RadiansPerSample   |                                   | ir   |
| rand                | Rand               | lo hi                             | ir   |
| sample-dur          | SampleDur          |                                   | ir   |
| sample-rate         | SampleRate         |                                   | ir   |
| shared-out          | SharedOut          | bus inputs                        | kr   |
| subsample-offset    | SubsampleOffset    |                                   | ir   |
| unpack1-fft         | Unpack1FFT         | c b bi wm                         | dr   |
| warp1               | Warp1              | b ptr fs ws envb ov wrr i         | ar   |

(as of 2022-08-16)
