#lang scribble/manual

@title{rsc3: racket/scheme/supercollider}

@table-of-contents[]

Partially regenerated documentation (sc3.schelp -> rsc3.scrbl)

@; ------------------------------------------------------------------------

@include-section["HelpSource/Help.scrbl"]
@include-section["HelpSource/Classes/classes.scrbl"]
@include-section["HelpSource/Guides/guides.scrbl"]
@include-section["HelpSource/Other/JITLibChanges3.7.scrbl"]
@include-section["HelpSource/Overviews/overviews.scrbl"]
@include-section["HelpSource/Reference/reference.scrbl"]
@include-section["HelpSource/Tutorials/Tutorial.scrbl"]
@; further "HelpSource/Tutorials/"

@index-section[]
