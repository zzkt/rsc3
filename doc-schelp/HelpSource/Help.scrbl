#lang scribble/manual
@(require (for-label racket))

@title{Help}
Documentation home@section{categories}
 Help

SuperCollider is an audio server, programming language, and IDE for sound synthesis and algorithmic composition.

@section{NOTE}
  link::Guides/News-3_8##News in SuperCollider version 3.8:: ::

@section{SECTION}
 Search and browse
@section{definitionlist}
 
## link::Search:: || Search all documents and methods
## link::Browse:: || Browse all documents by categories
::

@section{SECTION}
 Getting started

These are useful starting points for getting help on SuperCollider:

@section{definitionlist}
 
## link::Tutorials/Getting-Started/00-Getting-Started-With-SC##Getting Started tutorial series:: || Get started with SuperCollider
## link::Guides/Glossary:: || Glossary
## link::Guides/ClientVsServer:: || Explaining the client vs server architecture.
## link::Guides/More-On-Getting-Help:: || How to find more help
## link::Browse.html#Tutorials#All tutorials:: || Index of all help files categorized under "Tutorials."
::

@section{SECTION}
 Documentation indexes

@section{definitionlist}
 
## link::Overviews/Documents:: || Alphabetical index of all documents
## link::Overviews/Classes:: || Alphabetical index of all classes
## link::Overviews/@section{ClassTree}
  || All classes by inheritance tree
## link::Overviews/Methods:: || Alphabetical index of all methods
::

@section{SECTION}
 Licensing

SuperCollider is free software published under the GPL: link::Other/Licensing::.

These help files are published under the Creative Commons CC-BY-SA-3 license: link::Other/HelpDocsLicensing::.


