#lang scribble/manual
@(require (for-label racket))

@title{Streams}
 Patterns/Streams Help overview@section{categories}
  Streams-Patterns-Events
@section{related}
  Classes/Stream, Classes/Pattern

@section{section}
  Some tutorials

@section{definitionlist}
 
## link::Tutorials/A-Practical-Guide/PG_01_Introduction:: || A Practical Guide to Patterns
::

@section{definitionlist}
 
## link::Tutorials/Streams-Patterns-Events1:: || Streams & Routines
## link::Tutorials/Streams-Patterns-Events2:: || Patterns Introduction
## link::Tutorials/Streams-Patterns-Events3:: || ListPatterns
## link::Tutorials/Streams-Patterns-Events4:: || Environment & Event
## link::Tutorials/Streams-Patterns-Events5:: || Event.default
## link::Tutorials/Streams-Patterns-Events6:: || Parallel Patterns
## link::Tutorials/Streams-Patterns-Events7:: || Practical Considerations
::

@section{definitionlist}
 
## link::Classes/Pattern:: || Pattern class helpfile
::

@section{section}
  Specific classes

@section{subsection}
  ListPatterns

@section{classtree}
 ListPattern

@section{subsection}
  FilterPatterns

@section{classtree}
 FilterPattern

@section{subsection}
  event stream specific filter patterns

@section{list}
 
## link::Classes/Pset::
## link::Classes/Pfset::
## link::Classes/Pmul::
## link::Classes/Padd::
## link::Classes/Psetp::
## link::Classes/Pmulp::
## link::Classes/Paddp::
## link::Classes/Pfindur::
::

@section{subsection}
  other Patterns

@section{list}
 
## link::Classes/Pwhite::
## link::Classes/Pbrown::
## link::Classes/Ppatmod::
## link::Classes/Plazy::
## link::Classes/Pbind::
## link::Classes/PstepNadd::
## link::Classes/PstepNfunc::
::

@section{subsection}
  Streams

@section{list}
 
## link::Classes/BinaryOpStream::
## link::Classes/UnaryOpStream::
## link::Reference/EventStream::
## link::Classes/EventStreamPlayer::
::



