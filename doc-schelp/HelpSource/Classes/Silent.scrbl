#lang scribble/manual
@(require (for-label racket))

@title{Silent}
 Output silence.@section{categories}
   UGens>Generators>Single-value


@section{description}


Output silence.


@section{classmethods}
 

@section{method}
 ar

@section{argument}
 numChannels

Number of channels to output.



