#lang scribble/manual
@(require (for-label racket))

@title{FuncFilterPattern}
 Abstract class that filters a pattern using a function@section{related}
  Classes/FilterPattern
@section{categories}
  Streams-Patterns-Events>Patterns>Filter

@section{description}

This is an abstract class that filters a pattern using a function. See subclasses below:

@section{classtree}
 FuncFilterPattern



