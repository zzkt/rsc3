#lang scribble/manual
@(require (for-label racket))

@title{MaxLocalBufs}
 Set the maximum number of local buffers in a synth@section{categories}
  UGens>InOut

@section{description}

This class is used internally by LocalBuf, sets the maximum number of local buffers in a synth.



