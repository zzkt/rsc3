#lang scribble/manual
@(require (for-label racket))
@section{instancemethods}
 

@section{subsection}
  Extensions by SCDoc

@section{method}
  stripWhiteSpace
Strips whitespace at the beginning and end of the string.
@section{returns}
  The stripped string

@section{method}
  unixCmdGetStdOutLines
Like link::#-unixCmdGetStdOut:: but returns the lines in an Array instead.
@section{returns}
  an link::Classes/Array:: of each line of output



