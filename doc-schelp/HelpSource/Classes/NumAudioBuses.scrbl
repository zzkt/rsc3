#lang scribble/manual
@(require (for-label racket))

@title{NumAudioBuses}
 Number of audio busses.@section{related}
  Classes/NumBuffers, Classes/NumControlBuses, Classes/NumInputBuses, Classes/NumOutputBuses, Classes/NumRunningSynths
@section{categories}
   UGens>Info


@section{description}


Number of audio busses.


@section{classmethods}
 

@section{method}
 ir



