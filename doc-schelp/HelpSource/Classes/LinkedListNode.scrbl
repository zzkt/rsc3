#lang scribble/manual
@(require (for-label racket))

@title{LinkedListNode}
 Internally used class for LinkedList nodes@section{categories}
  Collections

@section{description}

LinkedListNode is used to implement the internal nodes of the link::Classes/Linked@section{List}
  class.
You should not need to deal with a LinkedListNode directly.


