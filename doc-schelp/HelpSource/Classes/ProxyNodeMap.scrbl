#lang scribble/manual
@(require (for-label racket))

@title{ProxyNodeMap}
 store control values and bus mappings for NodeProxy@section{categories}
  Libraries>JITLib>NodeProxy
@section{related}
  Classes/Bus

@section{description}

Object to store control values and bus mappings independantly of a specific NodeProxy.



