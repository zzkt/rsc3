#lang scribble/manual
@(require (for-label racket))

@title{OnError}
 register functions to be evaluated when an error occurs@section{related}
  Classes/StartUp, Classes/ShutDown, Classes/ServerQuit, Classes/ServerTree, Classes/CmdPeriod
@section{categories}
  Control

@section{description}

OnError registers functions to perform an action when an error occurs.

@section{ClassMethods}
 

@section{method}
 run
Call the object in order.


