#lang scribble/manual
@(require (for-label racket))

@title{FilterPattern}
 abstract class that holds a pattern to be modified@section{related}
  Classes/ListPattern
@section{categories}
  Streams-Patterns-Events>Patterns>Filter

@section{Examples}
 


@racketblock[
// post subclasses:

FilterPattern.dumpClassSubtree;
::
]


