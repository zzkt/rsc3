#lang scribble/manual
@(require (for-label racket))

@title{A2K}
 Audio to control rate converter.@section{related}
  Classes/K2A
@section{categories}
   UGens>Conversion


@section{description}


Audio to control rate converter. Only needed in specific cases.


@section{classmethods}
 

@section{method}
 kr

@section{argument}
 in
The input signal.



