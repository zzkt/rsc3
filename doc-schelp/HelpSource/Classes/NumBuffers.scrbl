#lang scribble/manual
@(require (for-label racket))

@title{NumBuffers}
 Number of open buffers.@section{related}
  Classes/NumAudioBuses, Classes/NumControlBuses, Classes/NumInputBuses, Classes/NumOutputBuses, Classes/NumRunningSynths
@section{categories}
   UGens>Info


@section{description}


Number of open buffers.


@section{classmethods}
 

@section{method}
 ir



