#lang scribble/manual
@(require (for-label racket))

@title{RawArray}
@section{categories}
 Collections>Ordered
 Abstract superclass for arrays holding raw data values
@section{description}

RawArray is the abstract superclass of a group of array classes that hold raw data values.

@section{INSTANCEMETHODS}
 

@section{private}
 archiveAsCompileString, archiveAsObject


