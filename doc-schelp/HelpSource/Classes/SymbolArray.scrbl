#lang scribble/manual
@(require (for-label racket))

@title{SymbolArray}
an array whose indexed slots are all of the same type@section{related}
 Classes/Int8Array, Classes/Int16Array, Classes/Int32Array, Classes/FloatArray, Classes/DoubleArray
@section{categories}
 Collections>Ordered

@section{description}

These classes implement arrays whose indexed slots are all of the same type.
@section{list}
 
## Int8Array - 8 bit integer
## Int16Array - 16 bit integer
## Int32Array - 32 bit integer
## FloatArray - 32 bit floating point
## DoubleArray - 64 bit floating point
## SymbolArray - symbols
::

@section{INSTANCEMETHODS}
 

@section{method}
 readFromStream


