#lang scribble/manual
@(require (for-label racket))

@title{TaskProxyGui}
 a superclass for Guis for PatternProxies@section{categories}
  Libraries>JITLib>GUI, Live Coding
@section{related}
  Classes/TdefGui, Classes/PdefGui

@section{description}


Please see link::Classes/TdefGui:: and link::Classes/PdefGui:: for examples!


