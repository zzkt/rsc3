#lang scribble/manual
@(require (for-label racket))

@title{TDuty_old}
 Deprecated ugen@section{related}
  Classes/TDuty
@section{categories}
  UGens>Deprecated

@section{description}

This class is deprecated, use link::Classes/TDuty:: instead.



