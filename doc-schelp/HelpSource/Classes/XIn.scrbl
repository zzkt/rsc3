#lang scribble/manual
@(require (for-label racket))

@title{XIn}
 Helper class used by InBus.@section{categories}
  UGens>Input
@section{related}
  Classes/InBus, Classes/In, Classes/InFeedback, Classes/XInFeedback

@section{description}

A private class used in the implementation of link::Classes/InBus::.
You should never need to use this UGen directly.





