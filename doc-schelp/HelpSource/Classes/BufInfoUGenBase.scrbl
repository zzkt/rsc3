#lang scribble/manual
@(require (for-label racket))

@title{BufInfoUGenBase}
@section{categories}
  UGens>Base
 Base class for buffer info ugens
@section{description}

This is the superclass for the various buffer info ugens.



