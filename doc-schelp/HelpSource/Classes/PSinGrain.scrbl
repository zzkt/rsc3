#lang scribble/manual
@(require (for-label racket))

@title{PSinGrain}
 Very fast sine grain with a parabolic envelope@section{categories}
   UGens>Generators>Deterministic


@section{description}


Very fast sine grain with a parabolic envelope.


@section{classmethods}
 
@section{private}
  categories

@section{method}
 ar

@section{argument}
 freq

Frequency in Hertz.


@section{argument}
 dur

Grain duration.


@section{argument}
 amp

Grain amplitude.



