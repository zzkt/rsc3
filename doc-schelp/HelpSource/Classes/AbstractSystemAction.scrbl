#lang scribble/manual
@(require (for-label racket))

@title{AbstractSystemAction}
 register actions to be taken for system events@section{related}
  Classes/CmdPeriod, Classes/StartUp, Classes/ShutDown, Classes/ServerBoot, Classes/ServerTree, Classes/ServerQuit
@section{categories}
  Control


@section{ClassMethods}
 

@section{method}
 removeAll
Remove all items from registry.


