#lang scribble/manual
@(require (for-label racket))

@title{NumInputBuses}
 Number of input busses.@section{related}
  Classes/NumAudioBuses, Classes/NumControlBuses, Classes/NumBuffers, Classes/NumOutputBuses, Classes/NumRunningSynths
@section{categories}
   UGens>Info


@section{description}


Number of input busses.


@section{classmethods}
 

@section{method}
 ir



