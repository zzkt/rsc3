#lang scribble/manual
@(require (for-label racket))

@title{NumControlBuses}
 Number of control busses.@section{related}
  Classes/NumAudioBuses, Classes/NumBuffers, Classes/NumInputBuses, Classes/NumOutputBuses, Classes/NumRunningSynths
@section{categories}
   UGens>Info


@section{description}


Number of control busses.


@section{classmethods}
 

@section{method}
 ir



