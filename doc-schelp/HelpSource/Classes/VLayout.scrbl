#lang scribble/manual
@(require (for-label racket))

@title{VLayout}
 A layout that distributes views in a vertical line@section{categories}
  GUI>Layout
@section{related}
  Classes/HLayout, Classes/GridLayout, Classes/StackLayout, Guides/GUI-Layout-Management

@section{description}

See documentation of superclass link::Classes/LineLayout:: for details.

@section{CLASSMETHODS}
 

@section{PRIVATE}
  key
@section{PRIVATE}
  layoutClass


