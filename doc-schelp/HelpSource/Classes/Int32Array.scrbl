#lang scribble/manual
@(require (for-label racket))

@title{Int32Array}
an array whose indexed slots are all of the same type@section{related}
 Classes/FloatArray, Classes/Int8Array, Classes/Int16Array, Classes/DoubleArray, Classes/SymbolArray
@section{categories}
 Collections>Ordered

@section{description}

These classes implement arrays whose indexed slots are all of the same type.
@section{list}
 
## Int8Array - 8 bit integer
## Int16Array - 16 bit integer
## Int32Array - 32 bit integer
## FloatArray - 32 bit floating point
## DoubleArray - 64 bit floating point
## SymbolArray - symbols
::

@section{INSTANCEMETHODS}
 

@section{method}
 readFromStream


