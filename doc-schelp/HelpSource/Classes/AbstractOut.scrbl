#lang scribble/manual
@(require (for-label racket))

@title{AbstractOut}
 Abstract class for out ugens@section{categories}
  UGens>InOut

@section{classmethods}
 
@section{private}
  categories

@section{instancemethods}
 
@section{method}
  numOutputs
@section{returns}
 
number of output buses (default: 0, overridden in subclasses)



