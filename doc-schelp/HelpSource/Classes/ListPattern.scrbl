#lang scribble/manual
@(require (for-label racket))

@title{ListPattern}
 abstract class that holds a list@section{related}
  Classes/FilterPattern
@section{categories}
  Streams-Patterns-Events>Patterns>List

@section{Examples}
 


@racketblock[
// post subclasses:

ListPattern.dumpClassSubtree;
::
]


