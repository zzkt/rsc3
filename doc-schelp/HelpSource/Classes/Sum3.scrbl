#lang scribble/manual
@(require (for-label racket))

@title{Sum3}
 Sum three signals@section{categories}
  UGens>Algebraic
@section{related}
  Classes/Mix

@section{description}

Sum three signals. Internal ugen to efficiently mix three signals. Should be used via link::Classes/Mix::.

@section{CLASSMETHODS}
 
@section{private}
  new1

@section{METHOD}
  new

Construct UGen.


