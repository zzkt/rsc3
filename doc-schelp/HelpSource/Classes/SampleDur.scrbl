#lang scribble/manual
@(require (for-label racket))

@title{SampleDur}
 Duration of one sample.@section{related}
  Classes/ControlRate, Classes/RadiansPerSample, Classes/SampleRate, Classes/SubsampleOffset
@section{categories}
   UGens>Info


@section{description}


Returns the current sample duration of the server.
Equivalent to 1/SampleRate.


@section{classmethods}
 

@section{method}
 ir



