#lang scribble/manual
@(require (for-label racket))

@title{ScIDE}
 interaction with the Qt IDE@section{categories}
  Frontends
@section{related}
  Classes/Document

@section{description}


The ScIDE class contains a number of class methods for interaction with the Qt IDE. Most users won't need to access these methods directly. For example, the link::Classes/Document:: class wraps most of the document-related functionality.

There's no use calling *new on this class. It has no instance methods!

