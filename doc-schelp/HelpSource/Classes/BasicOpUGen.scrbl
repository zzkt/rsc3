#lang scribble/manual
@(require (for-label racket))

@title{BasicOpUGen}
 Common superclass to operations on UGen@section{categories}
  UGens>Algebraic
@section{related}
  Classes/UnaryOpUGen, Classes/BinaryOpUGen, Overviews/Operators

@section{description}

Common superclass to operations on UGens

@section{instancemethods}
 

@section{method}
 operator
set or get the operator



