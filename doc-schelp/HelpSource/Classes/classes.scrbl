#lang scribble/manual

@title{Classes}

@include-section["A2K.scrbl"]
@include-section["APF.scrbl"]
@include-section["AbstractDispatcher.scrbl"]
@include-section["AbstractFunction.scrbl"]
@include-section["AbstractIn.scrbl"]
@include-section["AbstractMessageMatcher.scrbl"]
@include-section["AbstractOut.scrbl"]
@include-section["AbstractResponderFunc.scrbl"]
@include-section["AbstractSystemAction.scrbl"]
@include-section["AbstractWrappingDispatcher.scrbl"]
@include-section["AllpassC.scrbl"]
@include-section["AllpassL.scrbl"]
@include-section["AllpassN.scrbl"]
@include-section["AmpComp.scrbl"]
@include-section["AmpCompA.scrbl"]
@include-section["Amplitude.scrbl"]
@include-section["AppClock.scrbl"]
@include-section["ApplicationStart.scrbl"]
@include-section["Archive.scrbl"]
@include-section["Array.scrbl"]
@include-section["Array2D.scrbl"]
@include-section["ArrayedCollection.scrbl"]
@include-section["Association.scrbl"]
@include-section["AudioIn.scrbl"]
@include-section["BAllPass.scrbl"]
@include-section["BBandPass.scrbl"]
@include-section["BBandStop.scrbl"]
@include-section["BEQSuite.scrbl"]
@include-section["BHiPass.scrbl"]
@include-section["BHiPass4.scrbl"]
@include-section["BHiShelf.scrbl"]
@include-section["BLowPass.scrbl"]
@include-section["BLowPass4.scrbl"]
@include-section["BLowShelf.scrbl"]
@include-section["BPF.scrbl"]
@include-section["BPZ2.scrbl"]
@include-section["BPeakEQ.scrbl"]
@include-section["BRF.scrbl"]
@include-section["BRZ2.scrbl"]
@include-section["Bag.scrbl"]
@include-section["Balance2.scrbl"]
@include-section["Ball.scrbl"]
@include-section["BasicOpUGen.scrbl"]
@include-section["BeatTrack.scrbl"]
@include-section["BeatTrack2.scrbl"]
@include-section["BendResponder.scrbl"]
@include-section["BiPanB2.scrbl"]
@include-section["BinaryOpFunction.scrbl"]
@include-section["BinaryOpStream.scrbl"]
@include-section["BinaryOpUGen.scrbl"]
@include-section["Blip.scrbl"]
@include-section["Boolean.scrbl"]
@include-section["BrownNoise.scrbl"]
@include-section["BufAllpassC.scrbl"]
@include-section["BufAllpassL.scrbl"]
@include-section["BufAllpassN.scrbl"]
@include-section["BufChannels.scrbl"]
@include-section["BufCombC.scrbl"]
@include-section["BufCombL.scrbl"]
@include-section["BufCombN.scrbl"]
@include-section["BufDelayC.scrbl"]
@include-section["BufDelayL.scrbl"]
@include-section["BufDelayN.scrbl"]
@include-section["BufDur.scrbl"]
@include-section["BufFrames.scrbl"]
@include-section["BufInfoUGenBase.scrbl"]
@include-section["BufRateScale.scrbl"]
@include-section["BufRd.scrbl"]
@include-section["BufSampleRate.scrbl"]
@include-section["BufSamples.scrbl"]
@include-section["BufWr.scrbl"]
@include-section["Buffer.scrbl"]
@include-section["Bus.scrbl"]
@include-section["BusPlug.scrbl"]
@include-section["Button.scrbl"]
@include-section["CCResponder.scrbl"]
@include-section["COsc.scrbl"]
@include-section["CSVFileReader.scrbl"]
@include-section["Changed.scrbl"]
@include-section["Char.scrbl"]
@include-section["CheckBadValues.scrbl"]
@include-section["CheckBox.scrbl"]
@include-section["Class.scrbl"]
@include-section["Clip.scrbl"]
@include-section["ClipNoise.scrbl"]
@include-section["Clock.scrbl"]
@include-section["CmdPeriod.scrbl"]
@include-section["CoinGate.scrbl"]
@include-section["Collection.scrbl"]
@include-section["Color.scrbl"]
@include-section["CombC.scrbl"]
@include-section["CombL.scrbl"]
@include-section["CombN.scrbl"]
@include-section["Compander.scrbl"]
@include-section["CompanderD.scrbl"]
@include-section["Complex.scrbl"]
@include-section["CompositeView.scrbl"]
@include-section["Condition.scrbl"]
@include-section["ContiguousBlockAllocator.scrbl"]
@include-section["Control.scrbl"]
@include-section["ControlDur.scrbl"]
@include-section["ControlName.scrbl"]
@include-section["ControlRate.scrbl"]
@include-section["ControlSpec.scrbl"]
@include-section["Convolution.scrbl"]
@include-section["Convolution2.scrbl"]
@include-section["Convolution2L.scrbl"]
@include-section["Convolution3.scrbl"]
@include-section["Crackle.scrbl"]
@include-section["CuspL.scrbl"]
@include-section["CuspN.scrbl"]
@include-section["DC.scrbl"]
@include-section["Date.scrbl"]
@include-section["Dbrown.scrbl"]
@include-section["Dbufrd.scrbl"]
@include-section["Dbufwr.scrbl"]
@include-section["Dconst.scrbl"]
@include-section["DebugNodeWatcher.scrbl"]
@include-section["Decay.scrbl"]
@include-section["Decay2.scrbl"]
@include-section["DecodeB2.scrbl"]
@include-section["DegreeToKey.scrbl"]
@include-section["DelTapRd.scrbl"]
@include-section["DelTapWr.scrbl"]
@include-section["Delay1.scrbl"]
@include-section["Delay2.scrbl"]
@include-section["DelayC.scrbl"]
@include-section["DelayL.scrbl"]
@include-section["DelayN.scrbl"]
@include-section["Demand.scrbl"]
@include-section["DemandEnvGen.scrbl"]
@include-section["DetectIndex.scrbl"]
@include-section["DetectSilence.scrbl"]
@include-section["Dgeom.scrbl"]
@include-section["Dialog.scrbl"]
@include-section["Dibrown.scrbl"]
@include-section["Dictionary.scrbl"]
@include-section["DiskIn.scrbl"]
@include-section["DiskOut.scrbl"]
@include-section["Diwhite.scrbl"]
@include-section["Done.scrbl"]
@include-section["DoubleArray.scrbl"]
@include-section["Download.scrbl"]
@include-section["Dpoll.scrbl"]
@include-section["DragBoth.scrbl"]
@include-section["DragSink.scrbl"]
@include-section["DragSource.scrbl"]
@include-section["Drand.scrbl"]
@include-section["DrawGrid.scrbl"]
@include-section["Dreset.scrbl"]
@include-section["Dseq.scrbl"]
@include-section["Dser.scrbl"]
@include-section["Dseries.scrbl"]
@include-section["Dshuf.scrbl"]
@include-section["Dstutter.scrbl"]
@include-section["Dswitch.scrbl"]
@include-section["Dswitch1.scrbl"]
@include-section["Dunique.scrbl"]
@include-section["Dust.scrbl"]
@include-section["Dust2.scrbl"]
@include-section["Duty.scrbl"]
@include-section["Dwhite.scrbl"]
@include-section["Dwrand.scrbl"]
@include-section["Dxrand.scrbl"]
@include-section["DynKlang.scrbl"]
@include-section["DynKlank.scrbl"]
@include-section["EZGui.scrbl"]
@include-section["EZKnob.scrbl"]
@include-section["EZListView.scrbl"]
@include-section["EZLists.scrbl"]
@include-section["EZNumber.scrbl"]
@include-section["EZPopUpMenu.scrbl"]
@include-section["EZRanger.scrbl"]
@include-section["EZScroller.scrbl"]
@include-section["EZSlider.scrbl"]
@include-section["EZText.scrbl"]
@include-section["Env.scrbl"]
@include-section["EnvGate.scrbl"]
@include-section["EnvGen.scrbl"]
@include-section["EnvelopeView.scrbl"]
@include-section["EnvirGui.scrbl"]
@include-section["Environment.scrbl"]
@include-section["EnvironmentRedirect.scrbl"]
@include-section["Error.scrbl"]
@include-section["Event.scrbl"]
@include-section["EventPatternProxy.scrbl"]
@include-section["EventStreamCleanup.scrbl"]
@include-section["EventStreamPlayer.scrbl"]
@include-section["Exception.scrbl"]
@include-section["ExpRand.scrbl"]
@include-section["FBSineC.scrbl"]
@include-section["FBSineL.scrbl"]
@include-section["FBSineN.scrbl"]
@include-section["FFT.scrbl"]
@include-section["FFTTrigger.scrbl"]
@include-section["FOS.scrbl"]
@include-section["FSinOsc.scrbl"]
@include-section["False.scrbl"]
@include-section["Fdef.scrbl"]
@include-section["File.scrbl"]
@include-section["FileDialog.scrbl"]
@include-section["FileReader.scrbl"]
@include-section["Filter.scrbl"]
@include-section["FilterPattern.scrbl"]
@include-section["Float.scrbl"]
@include-section["FloatArray.scrbl"]
@include-section["FlowLayout.scrbl"]
@include-section["FlowVar.scrbl"]
@include-section["FlowView.scrbl"]
@include-section["Fold.scrbl"]
@include-section["Font.scrbl"]
@include-section["Formant.scrbl"]
@include-section["Formlet.scrbl"]
@include-section["Frame.scrbl"]
@include-section["Free.scrbl"]
@include-section["FreeSelf.scrbl"]
@include-section["FreeSelfWhenDone.scrbl"]
@include-section["FreeVerb.scrbl"]
@include-section["FreeVerb2.scrbl"]
@include-section["FreqScope.scrbl"]
@include-section["FreqScopeView.scrbl"]
@include-section["FreqShift.scrbl"]
@include-section["FuncFilterPattern.scrbl"]
@include-section["Function.scrbl"]
@include-section["FunctionDef.scrbl"]
@include-section["FunctionList.scrbl"]
@include-section["GVerb.scrbl"]
@include-section["Gate.scrbl"]
@include-section["GbmanL.scrbl"]
@include-section["GbmanN.scrbl"]
@include-section["Gendy1.scrbl"]
@include-section["Gendy2.scrbl"]
@include-section["Gendy3.scrbl"]
@include-section["Git.scrbl"]
@include-section["Gradient.scrbl"]
@include-section["GrainBuf.scrbl"]
@include-section["GrainFM.scrbl"]
@include-section["GrainIn.scrbl"]
@include-section["GrainSin.scrbl"]
@include-section["GrayNoise.scrbl"]
@include-section["GridLayout.scrbl"]
@include-section["GridLines.scrbl"]
@include-section["Group.scrbl"]
@include-section["HID.scrbl"]
@include-section["HIDCollection.scrbl"]
@include-section["HIDElement.scrbl"]
@include-section["HIDElementProto.scrbl"]
@include-section["HIDFunc.scrbl"]
@include-section["HIDInfo.scrbl"]
@include-section["HIDProto.scrbl"]
@include-section["HIDUsage.scrbl"]
@include-section["HIDdef.scrbl"]
@include-section["HLayout.scrbl"]
@include-section["HLayoutView.scrbl"]
@include-section["HPF.scrbl"]
@include-section["HPZ1.scrbl"]
@include-section["HPZ2.scrbl"]
@include-section["Harmonics.scrbl"]
@include-section["Hasher.scrbl"]
@include-section["HelpBrowser.scrbl"]
@include-section["HenonC.scrbl"]
@include-section["HenonL.scrbl"]
@include-section["HenonN.scrbl"]
@include-section["Hilbert.scrbl"]
@include-section["HilbertFIR.scrbl"]
@include-section["HiliteGradient.scrbl"]
@include-section["History.scrbl"]
@include-section["HistoryGui.scrbl"]
@include-section["IEnvGen.scrbl"]
@include-section["IFFT.scrbl"]
@include-section["IODesc.scrbl"]
@include-section["IRand.scrbl"]
@include-section["IdentityBag.scrbl"]
@include-section["IdentityDictionary.scrbl"]
@include-section["IdentitySet.scrbl"]
@include-section["Image.scrbl"]
@include-section["Impulse.scrbl"]
@include-section["In.scrbl"]
@include-section["InBus.scrbl"]
@include-section["InFeedback.scrbl"]
@include-section["InRange.scrbl"]
@include-section["InRect.scrbl"]
@include-section["InTrig.scrbl"]
@include-section["Index.scrbl"]
@include-section["IndexInBetween.scrbl"]
@include-section["IndexL.scrbl"]
@include-section["InfoUGenBase.scrbl"]
@include-section["Int16Array.scrbl"]
@include-section["Int32Array.scrbl"]
@include-section["Int8Array.scrbl"]
@include-section["Integrator.scrbl"]
@include-section["InterplEnv.scrbl"]
@include-section["InterplPairs.scrbl"]
@include-section["InterplXYC.scrbl"]
@include-section["Interpreter.scrbl"]
@include-section["Interval.scrbl"]
@include-section["JITGui.scrbl"]
@include-section["K2A.scrbl"]
@include-section["KeyState.scrbl"]
@include-section["KeyTrack.scrbl"]
@include-section["Klang.scrbl"]
@include-section["Knob.scrbl"]
@include-section["LFClipNoise.scrbl"]
@include-section["LFCub.scrbl"]
@include-section["LFDClipNoise.scrbl"]
@include-section["LFDNoise0.scrbl"]
@include-section["LFDNoise1.scrbl"]
@include-section["LFDNoise3.scrbl"]
@include-section["LFGauss.scrbl"]
@include-section["LFNoise0.scrbl"]
@include-section["LFNoise1.scrbl"]
@include-section["LFNoise2.scrbl"]
@include-section["LFPar.scrbl"]
@include-section["LFPulse.scrbl"]
@include-section["LFSaw.scrbl"]
@include-section["LFTri.scrbl"]
@include-section["LID.scrbl"]
@include-section["LIDGui.scrbl"]
@include-section["LIDInfo.scrbl"]
@include-section["LIDSlot.scrbl"]
@include-section["LPF.scrbl"]
@include-section["LPZ1.scrbl"]
@include-section["LPZ2.scrbl"]
@include-section["Lag.scrbl"]
@include-section["Lag2.scrbl"]
@include-section["Lag2UD.scrbl"]
@include-section["Lag3.scrbl"]
@include-section["Lag3UD.scrbl"]
@include-section["LagControl.scrbl"]
@include-section["LagIn.scrbl"]
@include-section["LagUD.scrbl"]
@include-section["LanguageConfig.scrbl"]
@include-section["LastValue.scrbl"]
@include-section["Latch.scrbl"]
@include-section["LatoocarfianC.scrbl"]
@include-section["LatoocarfianL.scrbl"]
@include-section["LatoocarfianN.scrbl"]
@include-section["Layout.scrbl"]
@include-section["LazyEnvir.scrbl"]
@include-section["LeakDC.scrbl"]
@include-section["LeastChange.scrbl"]
@include-section["LevelIndicator.scrbl"]
@include-section["Library.scrbl"]
@include-section["LibraryBase.scrbl"]
@include-section["Limiter.scrbl"]
@include-section["LinCongC.scrbl"]
@include-section["LinCongL.scrbl"]
@include-section["LinCongN.scrbl"]
@include-section["LinExp.scrbl"]
@include-section["LinLin.scrbl"]
@include-section["LinPan2.scrbl"]
@include-section["LinRand.scrbl"]
@include-section["LinSelectX.scrbl"]
@include-section["LinXFade2.scrbl"]
@include-section["Line.scrbl"]
@include-section["LineLayout.scrbl"]
@include-section["Linen.scrbl"]
@include-section["LinkedList.scrbl"]
@include-section["LinkedListNode.scrbl"]
@include-section["List.scrbl"]
@include-section["ListPattern.scrbl"]
@include-section["ListView.scrbl"]
@include-section["LocalBuf.scrbl"]
@include-section["LocalIn.scrbl"]
@include-section["LocalOut.scrbl"]
@include-section["Logistic.scrbl"]
@include-section["LorenzL.scrbl"]
@include-section["Loudness.scrbl"]
@include-section["MFCC.scrbl"]
@include-section["MIDIClient.scrbl"]
@include-section["MIDIFunc.scrbl"]
@include-section["MIDIFuncBothCAMessageMatcher.scrbl"]
@include-section["MIDIFuncBothMessageMatcher.scrbl"]
@include-section["MIDIFuncChanArrayMessageMatcher.scrbl"]
@include-section["MIDIFuncChanMessageMatcher.scrbl"]
@include-section["MIDIFuncSrcMessageMatcher.scrbl"]
@include-section["MIDIFuncSrcMessageMatcherNV.scrbl"]
@include-section["MIDIIn.scrbl"]
@include-section["MIDIMessageDispatcher.scrbl"]
@include-section["MIDIMessageDispatcherNV.scrbl"]
@include-section["MIDIOut.scrbl"]
@include-section["MIDIResponder.scrbl"]
@include-section["MIDIdef.scrbl"]
@include-section["Magnitude.scrbl"]
@include-section["Main.scrbl"]
@include-section["MantissaMask.scrbl"]
@include-section["MaxLocalBufs.scrbl"]
@include-section["Maybe.scrbl"]
@include-section["Median.scrbl"]
@include-section["Message.scrbl"]
@include-section["MidEQ.scrbl"]
@include-section["Mix.scrbl"]
@include-section["ModDif.scrbl"]
@include-section["Monitor.scrbl"]
@include-section["MonitorGui.scrbl"]
@include-section["MoogFF.scrbl"]
@include-section["MostChange.scrbl"]
@include-section["MouseButton.scrbl"]
@include-section["MouseX.scrbl"]
@include-section["MouseY.scrbl"]
@include-section["MulAdd.scrbl"]
@include-section["MultiLevelIdentityDictionary.scrbl"]
@include-section["MultiOutUGen.scrbl"]
@include-section["MultiSliderView.scrbl"]
@include-section["MultiTap.scrbl"]
@include-section["NAryOpFunction.scrbl"]
@include-section["NAryOpStream.scrbl"]
@include-section["NRand.scrbl"]
@include-section["NamedControl.scrbl"]
@include-section["Ndef.scrbl"]
@include-section["NdefGui.scrbl"]
@include-section["NdefMixer.scrbl"]
@include-section["NdefMixerOld.scrbl"]
@include-section["NdefParamGui.scrbl"]
@include-section["NetAddr.scrbl"]
@include-section["Nil.scrbl"]
@include-section["NodeControl.scrbl"]
@include-section["NodeMap.scrbl"]
@include-section["NodeProxy.scrbl"]
@include-section["NodeProxyEditor.scrbl"]
@include-section["NodeWatcher.scrbl"]
@include-section["Normalizer.scrbl"]
@include-section["NoteOffResponder.scrbl"]
@include-section["NoteOnResponder.scrbl"]
@include-section["NotificationCenter.scrbl"]
@include-section["NumAudioBuses.scrbl"]
@include-section["NumBuffers.scrbl"]
@include-section["NumChannels.scrbl"]
@include-section["NumControlBuses.scrbl"]
@include-section["NumInputBuses.scrbl"]
@include-section["NumOutputBuses.scrbl"]
@include-section["NumRunningSynths.scrbl"]
@include-section["Number.scrbl"]
@include-section["NumberBox.scrbl"]
@include-section["OSCArgsMatcher.scrbl"]
@include-section["OSCBundle.scrbl"]
@include-section["OSCFunc.scrbl"]
@include-section["OSCFuncAddrMessageMatcher.scrbl"]
@include-section["OSCFuncBothMessageMatcher.scrbl"]
@include-section["OSCFuncRecvPortMessageMatcher.scrbl"]
@include-section["OSCMessageDispatcher.scrbl"]
@include-section["OSCMessagePatternDispatcher.scrbl"]
@include-section["OSCdef.scrbl"]
@include-section["OSCpathResponder.scrbl"]
@include-section["OSCresponder.scrbl"]
@include-section["OSCresponderNode.scrbl"]
@include-section["Object.scrbl"]
@include-section["ObjectGui.scrbl"]
@include-section["ObjectTable.scrbl"]
@include-section["OffsetOut.scrbl"]
@include-section["OnError.scrbl"]
@include-section["OnePole.scrbl"]
@include-section["OneZero.scrbl"]
@include-section["Onsets.scrbl"]
@include-section["Order.scrbl"]
@include-section["OrderedIdentitySet.scrbl"]
@include-section["Osc.scrbl"]
@include-section["OscN.scrbl"]
@include-section["Out.scrbl"]
@include-section["OutputProxy.scrbl"]
@include-section["PMOsc.scrbl"]
@include-section["PSinGrain.scrbl"]
@include-section["PV_Add.scrbl"]
@include-section["PV_BinScramble.scrbl"]
@include-section["PV_BinShift.scrbl"]
@include-section["PV_BinWipe.scrbl"]
@include-section["PV_BrickWall.scrbl"]
@include-section["PV_ChainUGen.scrbl"]
@include-section["PV_ConformalMap.scrbl"]
@include-section["PV_Conj.scrbl"]
@include-section["PV_Copy.scrbl"]
@include-section["PV_CopyPhase.scrbl"]
@include-section["PV_Diffuser.scrbl"]
@include-section["PV_Div.scrbl"]
@include-section["PV_HainsworthFoote.scrbl"]
@include-section["PV_JensenAndersen.scrbl"]
@include-section["PV_LocalMax.scrbl"]
@include-section["PV_MagAbove.scrbl"]
@include-section["PV_MagBelow.scrbl"]
@include-section["PV_MagClip.scrbl"]
@include-section["PV_MagDiv.scrbl"]
@include-section["PV_MagFreeze.scrbl"]
@include-section["PV_MagMul.scrbl"]
@include-section["PV_MagNoise.scrbl"]
@include-section["PV_MagShift.scrbl"]
@include-section["PV_MagSmear.scrbl"]
@include-section["PV_MagSquared.scrbl"]
@include-section["PV_Max.scrbl"]
@include-section["PV_Min.scrbl"]
@include-section["PV_Mul.scrbl"]
@include-section["PV_PhaseShift.scrbl"]
@include-section["PV_PhaseShift270.scrbl"]
@include-section["PV_PhaseShift90.scrbl"]
@include-section["PV_RandComb.scrbl"]
@include-section["PV_RandWipe.scrbl"]
@include-section["PV_RectComb.scrbl"]
@include-section["PV_RectComb2.scrbl"]
@include-section["PackFFT.scrbl"]
@include-section["Padd.scrbl"]
@include-section["Paddp.scrbl"]
@include-section["Paddpre.scrbl"]
@include-section["PageLayout.scrbl"]
@include-section["Pair.scrbl"]
@include-section["Pan2.scrbl"]
@include-section["Pan4.scrbl"]
@include-section["PanAz.scrbl"]
@include-section["PanB.scrbl"]
@include-section["PanB2.scrbl"]
@include-section["ParGroup.scrbl"]
@include-section["ParamView.scrbl"]
@include-section["PartConv.scrbl"]
@include-section["PathName.scrbl"]
@include-section["Pattern.scrbl"]
@include-section["PatternConductor.scrbl"]
@include-section["PatternProxy.scrbl"]
@include-section["Pause.scrbl"]
@include-section["PauseSelf.scrbl"]
@include-section["PauseSelfWhenDone.scrbl"]
@include-section["Pavaroh.scrbl"]
@include-section["Pbeta.scrbl"]
@include-section["Pbind.scrbl"]
@include-section["PbindProxy.scrbl"]
@include-section["Pbindef.scrbl"]
@include-section["Pbindf.scrbl"]
@include-section["Pbinop.scrbl"]
@include-section["Pbrown.scrbl"]
@include-section["Pbus.scrbl"]
@include-section["Pcauchy.scrbl"]
@include-section["Pchain.scrbl"]
@include-section["Pclutch.scrbl"]
@include-section["Pcollect.scrbl"]
@include-section["Pconst.scrbl"]
@include-section["Pdef.scrbl"]
@include-section["PdefAllGui.scrbl"]
@include-section["PdefEditor.scrbl"]
@include-section["PdefGui.scrbl"]
@include-section["Pdefn.scrbl"]
@include-section["PdefnAllGui.scrbl"]
@include-section["PdefnGui.scrbl"]
@include-section["PdegreeToKey.scrbl"]
@include-section["Pdfsm.scrbl"]
@include-section["Pdict.scrbl"]
@include-section["PdurStutter.scrbl"]
@include-section["Peak.scrbl"]
@include-section["PeakFollower.scrbl"]
@include-section["Pen.scrbl"]
@include-section["Penvir.scrbl"]
@include-section["Pevent.scrbl"]
@include-section["Pexprand.scrbl"]
@include-section["Pfin.scrbl"]
@include-section["Pfindur.scrbl"]
@include-section["Pfinval.scrbl"]
@include-section["Pfset.scrbl"]
@include-section["Pfsm.scrbl"]
@include-section["Pfunc.scrbl"]
@include-section["Pfuncn.scrbl"]
@include-section["Pfx.scrbl"]
@include-section["Pfxb.scrbl"]
@include-section["Pgate.scrbl"]
@include-section["Pgauss.scrbl"]
@include-section["Pgbrown.scrbl"]
@include-section["Pgeom.scrbl"]
@include-section["Pgpar.scrbl"]
@include-section["Pgroup.scrbl"]
@include-section["Phasor.scrbl"]
@include-section["Phprand.scrbl"]
@include-section["Pif.scrbl"]
@include-section["Pindex.scrbl"]
@include-section["PingPong.scrbl"]
@include-section["PinkNoise.scrbl"]
@include-section["Pipe.scrbl"]
@include-section["Pitch.scrbl"]
@include-section["PitchShift.scrbl"]
@include-section["Pkey.scrbl"]
@include-section["Place.scrbl"]
@include-section["Plambda.scrbl"]
@include-section["Platform.scrbl"]
@include-section["PlayBuf.scrbl"]
@include-section["Plazy.scrbl"]
@include-section["PlazyEnvir.scrbl"]
@include-section["PlazyEnvirN.scrbl"]
@include-section["Plotter.scrbl"]
@include-section["Plprand.scrbl"]
@include-section["Pluck.scrbl"]
@include-section["Pmeanrand.scrbl"]
@include-section["Pmono.scrbl"]
@include-section["PmonoArtic.scrbl"]
@include-section["Pmul.scrbl"]
@include-section["Pmulp.scrbl"]
@include-section["Pmulpre.scrbl"]
@include-section["Pn.scrbl"]
@include-section["Pnaryop.scrbl"]
@include-section["Pnsym.scrbl"]
@include-section["Point.scrbl"]
@include-section["Polar.scrbl"]
@include-section["Poll.scrbl"]
@include-section["PopUpMenu.scrbl"]
@include-section["Post.scrbl"]
@include-section["Ppar.scrbl"]
@include-section["PparGroup.scrbl"]
@include-section["Ppatlace.scrbl"]
@include-section["Ppatmod.scrbl"]
@include-section["Ppoisson.scrbl"]
@include-section["Pprob.scrbl"]
@include-section["Pprotect.scrbl"]
@include-section["Pproto.scrbl"]
@include-section["Prand.scrbl"]
@include-section["Preject.scrbl"]
@include-section["Prewrite.scrbl"]
@include-section["PriorityQueue.scrbl"]
@include-section["Process.scrbl"]
@include-section["ProgramChangeResponder.scrbl"]
@include-section["Prorate.scrbl"]
@include-section["Prout.scrbl"]
@include-section["ProxyMixer.scrbl"]
@include-section["ProxyMixerOld.scrbl"]
@include-section["ProxyMonitorGui.scrbl"]
@include-section["ProxyNodeMap.scrbl"]
@include-section["ProxySpace.scrbl"]
@include-section["ProxySynthDef.scrbl"]
@include-section["Pseed.scrbl"]
@include-section["Pseg.scrbl"]
@include-section["Pselect.scrbl"]
@include-section["Pseq.scrbl"]
@include-section["Pser.scrbl"]
@include-section["Pseries.scrbl"]
@include-section["Pset.scrbl"]
@include-section["Psetp.scrbl"]
@include-section["Psetpre.scrbl"]
@include-section["Pshuf.scrbl"]
@include-section["Pslide.scrbl"]
@include-section["Pspawn.scrbl"]
@include-section["Pspawner.scrbl"]
@include-section["Pstep.scrbl"]
@include-section["PstepNadd.scrbl"]
@include-section["PstepNfunc.scrbl"]
@include-section["Pstutter.scrbl"]
@include-section["Pswitch.scrbl"]
@include-section["Pswitch1.scrbl"]
@include-section["Psym.scrbl"]
@include-section["Psync.scrbl"]
@include-section["Ptime.scrbl"]
@include-section["Ptpar.scrbl"]
@include-section["Ptuple.scrbl"]
@include-section["Pulse.scrbl"]
@include-section["PulseCount.scrbl"]
@include-section["PulseDivider.scrbl"]
@include-section["Punop.scrbl"]
@include-section["PureUGen.scrbl"]
@include-section["Pwalk.scrbl"]
@include-section["Pwhile.scrbl"]
@include-section["Pwhite.scrbl"]
@include-section["Pwrand.scrbl"]
@include-section["Pwrap.scrbl"]
@include-section["Pxrand.scrbl"]
@include-section["QPalette.scrbl"]
@include-section["QPenPrinter.scrbl"]
@include-section["QuadC.scrbl"]
@include-section["QuadL.scrbl"]
@include-section["QuadN.scrbl"]
@include-section["Quant.scrbl"]
@include-section["Quark.scrbl"]
@include-section["Quarks.scrbl"]
@include-section["QuartzComposerView.scrbl"]
@include-section["RHPF.scrbl"]
@include-section["RLPF.scrbl"]
@include-section["RadiansPerSample.scrbl"]
@include-section["Ramp.scrbl"]
@include-section["Rand.scrbl"]
@include-section["RandID.scrbl"]
@include-section["RandSeed.scrbl"]
@include-section["RangeSlider.scrbl"]
@include-section["RawArray.scrbl"]
@include-section["RawPointer.scrbl"]
@include-section["RecNodeProxy.scrbl"]
@include-section["RecordBuf.scrbl"]
@include-section["Recorder.scrbl"]
@include-section["Rect.scrbl"]
@include-section["Ref.scrbl"]
@include-section["RefCopy.scrbl"]
@include-section["ReplaceOut.scrbl"]
@include-section["Resonz.scrbl"]
@include-section["Rest.scrbl"]
@include-section["Ringz.scrbl"]
@include-section["RootNode.scrbl"]
@include-section["Rotate2.scrbl"]
@include-section["Routine.scrbl"]
@include-section["RunningMax.scrbl"]
@include-section["RunningMin.scrbl"]
@include-section["RunningSum.scrbl"]
@include-section["SCContainerView.scrbl"]
@include-section["SCDocHTMLRenderer.scrbl"]
@include-section["SCDocNode.scrbl"]
@include-section["SCDragView.scrbl"]
@include-section["SCEnvelopeEdit.scrbl"]
@include-section["SCImage.scrbl"]
@include-section["SCImageKernel.scrbl"]
@include-section["SCViewHolder.scrbl"]
@include-section["SOS.scrbl"]
@include-section["SampleDur.scrbl"]
@include-section["SampleRate.scrbl"]
@include-section["Sanitize.scrbl"]
@include-section["Saw.scrbl"]
@include-section["ScIDE.scrbl"]
@include-section["Scale.scrbl"]
@include-section["Scheduler.scrbl"]
@include-section["Schmidt.scrbl"]
@include-section["ScopeOut.scrbl"]
@include-section["ScopeView.scrbl"]
@include-section["Score.scrbl"]
@include-section["ScrollView.scrbl"]
@include-section["Select.scrbl"]
@include-section["SelectX.scrbl"]
@include-section["SelectXFocus.scrbl"]
@include-section["Semaphore.scrbl"]
@include-section["SemiColonFileReader.scrbl"]
@include-section["SendPeakRMS.scrbl"]
@include-section["SendReply.scrbl"]
@include-section["SendTrig.scrbl"]
@include-section["SerialPort.scrbl"]
@include-section["Server.scrbl"]
@include-section["ServerBoot.scrbl"]
@include-section["ServerMeter.scrbl"]
@include-section["ServerMeterView.scrbl"]
@include-section["ServerOptions.scrbl"]
@include-section["ServerQuit.scrbl"]
@include-section["ServerTree.scrbl"]
@include-section["Set.scrbl"]
@include-section["SetResetFF.scrbl"]
@include-section["Shaper.scrbl"]
@include-section["SharedIn.scrbl"]
@include-section["SharedOut.scrbl"]
@include-section["ShutDown.scrbl"]
@include-section["Signal.scrbl"]
@include-section["Silent.scrbl"]
@include-section["SimpleController.scrbl"]
@include-section["SimpleNumber.scrbl"]
@include-section["SinOsc.scrbl"]
@include-section["SinOscFB.scrbl"]
@include-section["SkipJack.scrbl"]
@include-section["Slew.scrbl"]
@include-section["Slider.scrbl"]
@include-section["Slider2D.scrbl"]
@include-section["Slope.scrbl"]
@include-section["SortedList.scrbl"]
@include-section["SoundFile.scrbl"]
@include-section["SoundFileView.scrbl"]
@include-section["SoundIn.scrbl"]
@include-section["SparseArray.scrbl"]
@include-section["SpecCentroid.scrbl"]
@include-section["SpecFlatness.scrbl"]
@include-section["SpecPcile.scrbl"]
@include-section["Speech.scrbl"]
@include-section["Splay.scrbl"]
@include-section["SplayAz.scrbl"]
@include-section["SplayZ.scrbl"]
@include-section["Spring.scrbl"]
@include-section["StackLayout.scrbl"]
@include-section["StandardL.scrbl"]
@include-section["StandardN.scrbl"]
@include-section["StaticText.scrbl"]
@include-section["Stepper.scrbl"]
@include-section["StereoConvolution2L.scrbl"]
@include-section["Stethoscope.scrbl"]
@include-section["Stream.scrbl"]
@include-section["StreamClutch.scrbl"]
@include-section["String.ext.scrbl"]
@include-section["String.scrbl"]
@include-section["SubsampleOffset.scrbl"]
@include-section["Sum3.scrbl"]
@include-section["Sum4.scrbl"]
@include-section["Sweep.scrbl"]
@include-section["Symbol.scrbl"]
@include-section["SymbolArray.scrbl"]
@include-section["SyncSaw.scrbl"]
@include-section["Synth.scrbl"]
@include-section["SynthDef.scrbl"]
@include-section["SynthDesc.scrbl"]
@include-section["SynthDescLib.scrbl"]
@include-section["SystemClock.scrbl"]
@include-section["T2A.scrbl"]
@include-section["T2K.scrbl"]
@include-section["TBall.scrbl"]
@include-section["TChoose.scrbl"]
@include-section["TDelay.scrbl"]
@include-section["TDuty.scrbl"]
@include-section["TDuty_old.scrbl"]
@include-section["TExpRand.scrbl"]
@include-section["TGrains.scrbl"]
@include-section["TIRand.scrbl"]
@include-section["TRand.scrbl"]
@include-section["TWChoose.scrbl"]
@include-section["TWindex.scrbl"]
@include-section["TabFileReader.scrbl"]
@include-section["Tap.scrbl"]
@include-section["Task.scrbl"]
@include-section["TaskProxy.scrbl"]
@include-section["TaskProxyGui.scrbl"]
@include-section["Tdef.scrbl"]
@include-section["TdefAllGui.scrbl"]
@include-section["TdefEditor.scrbl"]
@include-section["TdefGui.scrbl"]
@include-section["TempoBusClock.scrbl"]
@include-section["TempoClock.scrbl"]
@include-section["TextField.scrbl"]
@include-section["TextView.scrbl"]
@include-section["Thread.scrbl"]
@include-section["Thunk.scrbl"]
@include-section["Timer.scrbl"]
@include-section["ToggleFF.scrbl"]
@include-section["TouchResponder.scrbl"]
@include-section["TreeView.scrbl"]
@include-section["TreeViewItem.scrbl"]
@include-section["Trig.scrbl"]
@include-section["Trig1.scrbl"]
@include-section["TrigControl.scrbl"]
@include-section["True.scrbl"]
@include-section["Tuning.scrbl"]
@include-section["TwoPole.scrbl"]
@include-section["TwoWayIdentityDictionary.scrbl"]
@include-section["TwoZero.scrbl"]
@include-section["UGen.scrbl"]
@include-section["UnaryOpFunction.scrbl"]
@include-section["UnaryOpStream.scrbl"]
@include-section["UnaryOpUGen.scrbl"]
@include-section["UniqueID.scrbl"]
@include-section["UnixFILE.scrbl"]
@include-section["Unpack1FFT.scrbl"]
@include-section["UnpackFFT.scrbl"]
@include-section["UserView.scrbl"]
@include-section["VDiskIn.scrbl"]
@include-section["VLayout.scrbl"]
@include-section["VLayoutView.scrbl"]
@include-section["VOsc.scrbl"]
@include-section["VOsc3.scrbl"]
@include-section["VarLag.scrbl"]
@include-section["VarSaw.scrbl"]
@include-section["Vibrato.scrbl"]
@include-section["View.scrbl"]
@include-section["Volume.scrbl"]
@include-section["Warp.scrbl"]
@include-section["Warp1.scrbl"]
@include-section["Wavetable.scrbl"]
@include-section["WebView.scrbl"]
@include-section["WhiteNoise.scrbl"]
@include-section["WiiMote.scrbl"]
@include-section["Window.scrbl"]
@include-section["Wrap.scrbl"]
@include-section["WrapIndex.scrbl"]
@include-section["XFade2.scrbl"]
@include-section["XIn.scrbl"]
@include-section["XInFeedback.scrbl"]
@include-section["XLine.scrbl"]
@include-section["XOut.scrbl"]
@include-section["ZeroCrossing.scrbl"]
