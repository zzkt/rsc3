#lang scribble/manual
@(require (for-label racket))

@title{NumOutputBuses}
 Number of output busses.@section{related}
  Classes/NumAudioBuses, Classes/NumControlBuses, Classes/NumBuffers, Classes/NumInputBuses, Classes/NumRunningSynths
@section{categories}
   UGens>Info


@section{description}


Number of output busses.


@section{classmethods}
 

@section{method}
 ir



