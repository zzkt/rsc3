#lang scribble/manual
@(require (for-label racket))

@title{Sum4}
 Sum four signals@section{categories}
  UGens>Algebraic
@section{related}
  Classes/Mix

@section{description}

Sum four signals. Internal ugen to efficiently mix four signals. Should be used via link::Classes/Mix::.

@section{CLASSMETHODS}
 
@section{private}
  new1

@section{METHOD}
  new

Construct UGen.


