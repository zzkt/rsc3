#lang scribble/manual
@(require (for-label racket))

@title{RawPointer}
@section{categories}
 Core>Kernel
 Hold raw pointers from the host environment
@section{description}


A class used to hold raw pointers from the host environment.
No instance variables, no methods.



