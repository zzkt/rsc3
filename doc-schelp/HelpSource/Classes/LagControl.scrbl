#lang scribble/manual
@(require (for-label racket))

@title{LagControl}
 Lagged control input@section{related}
  Classes/Control, Classes/TrigControl
@section{categories}
   UGens>Synth control


@section{description}



@racketblock[Control:: ugen with fixed-time lags.

]
@section{classmethods}
 

@section{private}
 ir

@section{method}
 kr

@section{argument}
 values

Initial value (
@racketblock[Float:: or ]

@racketblock[Array:: of Floats).

]
@section{argument}
 lags

Lag times (
@racketblock[Float:: or ]

@racketblock[Array:: of Floats).


]


