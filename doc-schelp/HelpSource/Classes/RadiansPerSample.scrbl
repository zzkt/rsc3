#lang scribble/manual
@(require (for-label racket))

@title{RadiansPerSample}
 Number of radians per sample.@section{related}
  Classes/ControlRate, Classes/SampleDur, Classes/SampleRate, Classes/SubsampleOffset
@section{categories}
   UGens>Info


@section{description}


Returns the number of radians per sample.


@section{classmethods}
 

@section{method}
 ir



