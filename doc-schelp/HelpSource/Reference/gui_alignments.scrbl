#lang scribble/manual
@(require (for-label racket))

@title{Alignment}
 Alignment options within the GUI system@section{categories}
  GUI

Alignment in GUI classes is represented with the following symbols:

@section{list}
 
## \left
## \center
## \right
::

Additionally, the following symbols are available in Qt GUI:

@section{list}
 
## \topLeft
## \top
## \topRight
## \bottomLeft
## \bottom
## \bottomRight
::


